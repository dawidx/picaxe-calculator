main:
setup:
	let b3 = 0
	let b4 = 0
	let b5 = 0
	let b6 = 0
	let b8 = 0
	goto action
action: 	let b1 = pins
		if b1 > 00 then
			pause 1000
			let b2 = pins
			if b1 = $03 and b2 = $00 then
				let b8 = 0
			elseif b1 = $03 and b2 = $01 then
				let b8 = 1
			elseif b1 = $03 and b2 = $02 then
				let b8 = 2
			elseif b1 = $03 and b2 = $03 then
				let b8 = 3
			elseif b1 = $01 and b2 = $00 then
				let b8 = 4
			elseif b1 = $01 and b2 = $01 then
				let b8 = 5
			elseif b1 = $01 and b2 = $02 then
				let b8 = 6
			elseif b1 = $01 and b2 = $03 then
				let b8 = 7
			elseif b1 = $02 and b2 = $00 then
				let b8 = 8
			elseif b1 = $02 and b2 = $01 then
				let b8 = 9
			elseif b1 = $C0 and b2 = $00 then
				let b6 = 1
			elseif b1 = $C0 and b2 = $40 then
				let b6 = 2
			elseif b1 = $C0 and b2 = $80 then
				if b6 = 0 then 
					let b3 = 255 - b3
				else
					let b4 = 255 - b4 
				endif
			elseif b1 = $C0 and b2 = $C0 then
				goto solve
			endif
			
			if b6 = 0 then
				if b5 = 0 then
					b3 = b8
				else
					b3 = b3*10 + b8
				endif
				b5 = b5 + 1
				b7 = b3
				goto display
			else
				if b5 = 0 then
					b4 = b8
				else 
					b4 = b4*10 - b8
				endif
				b5 = b5 + 1
				b7 = b4
				goto display
			endif
		else goto action
		endif
solve:	if b6 = 1 then
			let b7 = b3+b4
			if b7 > 128 then
				b7 = $100
			endif
		else
			let b7 = b3-b4
			if b7 > 256 then
				b7 = $100
			endif
		endif
		let pins = b7
		goto setup
display:	
		let pins = b7
		goto action