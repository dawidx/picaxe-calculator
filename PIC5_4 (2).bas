main: goto setup
setup:
	let b3 = 0
	let b4 = 0
	let b5 = 0
	let b6 = $01
	let b8 = 0
	goto action
action: 	let b1 = pins
		if b1 > 0 then
			pause 10
			let b2 = pins
			pause 20
			if b1 = $03 and b2 = $00 then
				let b8 = 0
			elseif b1 = $03 and b2 = $01 then
				let b8 = 1
			elseif b1 = $03 and b2 = $02 then
				let b8 = 2
			elseif b1 = $03 and b2 = $03 then
				let b8 = 3
			elseif b1 = $01 and b2 = $00 then
				let b8 = 4
			elseif b1 = $01 and b2 = $01 then
				let b8 = 5
			elseif b1 = $01 and b2 = $02 then
				let b8 = 6
			elseif b1 = $01 and b2 = $03 then
				let b8 = 7
			elseif b1 = $02 and b2 = $00 then
				let b8 = 8
			elseif b1 = $02 and b2 = $02 then
				let b8 = 9
			elseif b1 = $C0 and b2 = $00 then
				let b6 = $02
				let b5 = 0			
				let b8 = 10
			elseif b1 = $C0 and b2 = $40 then
				let b6 = $03
				let b5 = 0
				let b8 = 10
			elseif b1 = $C0 and b2 = $80 then
				if b6 = $01 then 
					b3 = b3*255
				else
					b4 = b4*255
				endif
				let b8 = 11
			elseif b1 = $C0 and b2 = $C0 then
				goto solve
			endif
			if b8 < 10 then
				if b6 = $01 then
					if b5 = 0 then
						b3 = b8
					else
						b3 = b3*$0A + b8
					endif
					b5 = b5 + 1
					let pins = b3
				else
					if b5 = 0 then
						b4 = b8
					else 
						b4 = b4*$0A + b8
					endif
					b5 = b5 + 1
					let pins = b4
				endif
			elseif b8 = 10 then
				let pins = 0
			else
				if b6 = $01 then
					let pins = b3
				else 
					let pins = b4
				endif
			endif
			
			goto action
		else goto action
		endif
solve:	if b6 = $02 then
			let b7 = b3+b4
		else
			let b7 = b3-b4
		endif
		let pins = b7
		goto setup